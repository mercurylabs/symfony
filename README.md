# docker-smyfony

Docker-Compose based symfony images

### Build

If you plan to use a database, edit ```DATABASE_URL``` in ```.docker/nginx/.env```.

Then, with docker-compose installed, run ```docker-compose -f ./docker-compose.yml``` in project root.

### Run

Link your source and other neccessary volumes in docker-compose.yml. Then run ```docker-compose up -d``` in project root.
